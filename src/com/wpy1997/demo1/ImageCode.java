package com.wpy1997.demo1;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @Author: PengyuWang
 * @Description:
 * @Date: Created in 21:11 2021/8/6
 * @Modified By:
 */
public class ImageCode {
    static String[] strs = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "2",
            "3", "4", "5", "6", "7", "8", "9"};

    public static void main(String[] args) throws IOException {
        int w = 150;
        int h = 50;
        int imageType = 1;
        //ctrl + p 查看传入参数类型
        BufferedImage image = new BufferedImage(w, h, imageType);

        //把图片的颜色修改


        //获取画笔对象
        Graphics g = image.getGraphics();

        g.setColor(Color.cyan);
        g.fillRect(0, 0, w, h);
        Random random = new Random();
        int x = 30;
        int y = 30;
        g.setColor(Color.red);
        g.setFont(new Font("隶书",Font.PLAIN,25));
        for (int i = 0; i < 4; i++) {
            int num = random.nextInt(strs.length);
            String str = strs[num];
            //花图片

            g.drawString(str,x,y);
            x+=25;
        }


        //把image生成到本地磁盘
        ImageIO.write(image, "jpg", new File("D:\\aaa.jpg"));

    }

}

