package com.wpy1997.demo3;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * @Author: PengyuWang
 * @Description:
 * @Date: Created in 10:39 2021/8/8
 * @Modified By:
 */
public class PropertiesTest {
    public static void main(String[] args) throws IOException {
        Properties por = new Properties();
        FileReader fileReader = new FileReader("\\idea_java_projects\\gitstudy\\proTest.txt");
        por.load(fileReader);
        //File file = new File("\\idea_java_projects\\gitstudy\\proTest.txt");
        FileWriter fileWriter = new FileWriter("\\idea_java_projects\\gitstudy\\proTest.txt");
        fileReader.close();
        if (por.isEmpty()) {
            por.setProperty("count", "0");
            por.store(fileWriter, null);
            fileWriter.close();
        }else {


            int num = Integer.parseInt(por.getProperty("count"));
            if (num >= 3) {
                System.out.println("请充值“www.809787216.com”");

            } else {
                Game.startGame();
                num++;
                por.setProperty("count", String.valueOf(num));
                por.store(fileWriter, null);
            }
            fileWriter.close();
        }


    }
}
