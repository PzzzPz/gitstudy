package com.wpy1997.demo3;

import java.util.Scanner;

/**
 * @Author: PengyuWang
 * @Description:
 * @Date: Created in 10:40 2021/8/8
 * @Modified By:
 */
public class Game {
    public static void startGame(){
        System.out.println("请输入一个0-100的数：");
        Scanner scanner = new Scanner(System.in);

        int j = (int)(Math.random()*100);
        while (true){
            int i = scanner.nextInt();
            if (i<j){
                System.out.println("小了，请再次输入：");
            }else if (i>j){
                System.out.println("大了");
            }else{
                System.out.println("正确");
                break;
            }
        }
    }
}
