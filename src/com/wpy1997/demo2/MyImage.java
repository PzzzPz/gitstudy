package com.wpy1997.demo2;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @Author: PengyuWang
 * @Description:
 * @Date: Created in 15:33 2021/8/7
 * @Modified By:
 */

@SuppressWarnings({"all"})
public class MyImage extends JPanel {

    BufferedImage agImage;
    float ff = 0f;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D ge = (Graphics2D) g;
        if (agImage != null) {

            ge.setComposite(AlphaComposite.SrcOver.derive(ff / 100f));
            ge.drawImage(agImage, 0, 0, agImage.getWidth(), agImage.getHeight(), null);
        }

    }

    public static void main(String[] args) throws InterruptedException {
        JFrame frame = new JFrame();

        frame.setSize(750, 450);
        frame.setTitle("java电子相册");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(3);

        MyImage myImage = new MyImage();
        frame.add(myImage);
        frame.setVisible(true);

        myImage.initImages();

        //ctrl+o快捷键重写父类方法
        myImage.begin();


    }

    BufferedImage[] images = new BufferedImage[3];


    public void begin() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                int num = 0;
                while (true) {
                    agImage = images[num];
                    num++;
                    if (num == 3) {
                        num = 0;
                    }
                    while (true) {
                        if (ff < 100f) {
                            ff += 2f;
                            repaint();
                        } else {
                            ff = 0f;
                            break;
                        }
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }


    public void initImages() {
        for (int i = 1; i < 4; i++) {
            BufferedImage image = null;
            try {
                image = ImageIO.read(MyImage.class.getResource("/images/a" + i + ".jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            images[i - 1] = image;

        }


    }
}
